-- Creación de un usuario
CREATE USER userGRUD IDENTIFIED BY 'userGRUD123';
-- Otorgamiento de permisos de CRUD en la base de datos "Usuario"
GRANT SELECT, INSERT, UPDATE, DELETE ON Usuario.* TO userGRUD;
FLUSH PRIVILEGES;

-- Creación de la base de datos "Usuario"
CREATE DATABASE IF NOT EXISTS Usuario;
USE Usuario;

-- Creación de la tabla "usuarios"
CREATE TABLE IF NOT EXISTS usuarios (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    correo VARCHAR(255) NOT NULL UNIQUE,
    edad INT NOT NULL,
    genero VARCHAR(10) NOT NULL,
    direccion VARCHAR(255) NOT NULL,
    ciudad VARCHAR(100) NOT NULL,
    telefono VARCHAR(20) NOT NULL
    );

-- Inserción de datos de ejemplo
INSERT INTO usuarios (nombre, correo, edad, genero, direccion, ciudad, telefono)
VALUES
    ('Juan Pérez', 'juan@example.com', 30, 'Masculino', '123 Calle Principal', 'Ciudad A', '555-123-4567'),
    ('María López', 'maria@example.com', 28, 'Femenino', '456 Calle Secundaria', 'Ciudad B', '555-987-6543'),
    ('Carlos Rodríguez', 'carlos@example.com', 35, 'Masculino', '789 Calle Terciaria', 'Ciudad C', '555-456-7890');

SELECT * FROM usuarios;
