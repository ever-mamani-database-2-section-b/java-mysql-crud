package university.jala.conexionDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * La clase DatabaseConnection proporciona una conexión a una base de datos MySQL. Utiliza JDBC para
 * establecer la conexión con la base de datos.
 *
 * @author Ever Mamani Vicente.
 */
public class DatabaseConnection {

  private static final String URL = "jdbc:mysql://localhost:3306/Usuario";
  private static final String USER = "userGRUD";
  private static final String PASSWORD = "userGRUD123";

  private static Connection connection;

  /**
   * Obtiene una conexión a la base de datos.
   */
  private DatabaseConnection() {
    try {
      connection = DriverManager.getConnection(URL, USER, PASSWORD);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   *  Método estático para obtener la instancia única de la conexión
   */
  public static Connection getConnection() {
    if (connection == null) {
      new DatabaseConnection();
    }
    return connection;
  }

}
