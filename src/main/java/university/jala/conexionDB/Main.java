package university.jala.conexionDB;

/**
 * La clase principal del programa.
 *
 * @author Ever Mamani Vicente.
 */
public class Main {

  public static void main(String[] args) {

    UsuarioDAO usuarioDAO = new UsuarioDAO();

    Usuario nuevoUsuario = new Usuario("Daniel Torres", "daniel@example.com", 20, "Masculino",
        "Av. Siempre Viva 123", "La Paz", "555-311-654");

    //INSERT
    usuarioDAO.insertarUsuario(nuevoUsuario);

    Usuario usuario = usuarioDAO.obtenerUsuarioPorId(2);

    //UPDATE
    usuario.setCorreo("Modificado@example.com");
    usuario.setEdad(21);
    usuario.setCiudad("Cochabamba");
    usuarioDAO.actualizarUsuario(usuario);

    //DELETE
    usuarioDAO.eliminarUsuario(1);

    //SELECT
    usuarioDAO.getUsuarios();
  }
}