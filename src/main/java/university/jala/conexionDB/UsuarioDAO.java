package university.jala.conexionDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * La clase UsuarioDAO proporciona métodos para interactuar con la tabla de usuarios en la base de
 * datos. Permite realizar operaciones CRUD (Crear, Leer, Actualizar, Eliminar) en registros de
 * usuarios.
 *
 * @author Ever Mamani Vicente.
 */
public class UsuarioDAO {

  private final String INSERT_QUERY = "INSERT INTO usuarios (nombre, correo, edad, genero, direccion, ciudad, telefono) VALUES (?, ?, ?, ?, ?, ?, ?)";
  private final String UPDATE_QUERY = "UPDATE usuarios SET nombre=?, correo=?, edad=?, genero=?, direccion=?, ciudad=?, telefono=? WHERE id=?";
  private final String DELETE_QUERY = "DELETE FROM usuarios WHERE id=?";
  private final String SELECT_ALL_QUERY = "SELECT * FROM usuarios";
  private final String SELECT_BY_ID_QUERY = "SELECT * FROM usuarios WHERE id=?";

  private Connection connection;

  public UsuarioDAO() {
    connection = DatabaseConnection.getConnection();
  }

  /**
   * Inserta un nuevo usuario en la base de datos.
   *
   * @param usuario El objeto Usuario que se va a insertar.
   */
  public void insertarUsuario(Usuario usuario) {
    try (PreparedStatement stmt = connection.prepareStatement(INSERT_QUERY)) {
      stmt.setString(1, usuario.getNombre());
      stmt.setString(2, usuario.getCorreo());
      stmt.setInt(3, usuario.getEdad());
      stmt.setString(4, usuario.getGenero());
      stmt.setString(5, usuario.getDireccion());
      stmt.setString(6, usuario.getCiudad());
      stmt.setString(7, usuario.getTelefono());
      stmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * Actualiza la información de un usuario existente en la base de datos.
   *
   * @param usuario El objeto Usuario con la información actualizada.
   */
  public void actualizarUsuario(Usuario usuario) {
    try (PreparedStatement stmt = connection.prepareStatement(UPDATE_QUERY)) {
      stmt.setString(1, usuario.getNombre());
      stmt.setString(2, usuario.getCorreo());
      stmt.setInt(3, usuario.getEdad());
      stmt.setString(4, usuario.getGenero());
      stmt.setString(5, usuario.getDireccion());
      stmt.setString(6, usuario.getCiudad());
      stmt.setString(7, usuario.getTelefono());
      stmt.setInt(8, usuario.getId());
      stmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * Elimina un usuario de la base de datos por su ID.
   *
   * @param usuarioId El ID del usuario que se va a eliminar.
   */
  public void eliminarUsuario(int usuarioId) {
    try (PreparedStatement stmt = connection.prepareStatement(DELETE_QUERY)) {
      stmt.setInt(1, usuarioId);
      stmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * Obtiene un usuario de la base de datos por su ID.
   *
   * @param id El ID del usuario que se desea obtener.
   * @return El objeto Usuario correspondiente al ID proporcionado.
   */
  public Usuario obtenerUsuarioPorId(int id) {
    Usuario usuario = null;
    try (PreparedStatement stmt = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        usuario = new Usuario(rs.getInt("id"), rs.getString("nombre"), rs.getString("correo"),
            rs.getInt("edad"), rs.getString("genero"), rs.getString("direccion"),
            rs.getString("ciudad"), rs.getString("telefono"));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return usuario;
  }

  /**
   * Obtiene todos los usuarios de la base de datos y muestra su información en la consola.
   */
  public void getUsuarios() {
    try (PreparedStatement stmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        System.out.printf("ID: %d%n", rs.getInt("id"));
        System.out.printf("Nombre: %s%n", rs.getString("nombre"));
        System.out.printf("Correo: %s%n", rs.getString("correo"));
        System.out.printf("Edad: %d%n", rs.getInt("edad"));
        System.out.printf("Genero: %s%n", rs.getString("genero"));
        System.out.printf("Direccion: %s%n", rs.getString("direccion"));
        System.out.printf("Ciudad: %s%n", rs.getString("ciudad"));
        System.out.printf("Telefono: %s%n", rs.getString("telefono"));
        System.out.println("===================================");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
