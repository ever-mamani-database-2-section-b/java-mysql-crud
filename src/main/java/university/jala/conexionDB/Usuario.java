package university.jala.conexionDB;

/**
 * La clase Usuario representa un registro de la tabla usuarios.
 *
 * @author Ever Mamani Vicente.
 */
public class Usuario {

  private int id;
  private String nombre;
  private String correo;
  private int edad;
  private String genero;
  private String direccion;
  private String ciudad;
  private String telefono;

  /**
   * Constructor para crear un nuevo usuario
   */
  public Usuario(String nombre, String correo, int edad, String genero, String direccion,
      String ciudad, String telefono) {
    this.nombre = nombre;
    this.correo = correo;
    this.edad = edad;
    this.genero = genero;
    this.direccion = direccion;
    this.ciudad = ciudad;
    this.telefono = telefono;
  }

  /**
   * Constructor para cargar un usuario existente desde la base de datos
   */
  public Usuario(int id, String nombre, String correo, int edad, String genero, String direccion,
      String ciudad, String telefono) {
    this.id = id;
    this.nombre = nombre;
    this.correo = correo;
    this.edad = edad;
    this.genero = genero;
    this.direccion = direccion;
    this.ciudad = ciudad;
    this.telefono = telefono;
  }

  public int getId() {
    return id;
  }

  public String getNombre() {
    return nombre;
  }

  public String getCorreo() {
    return correo;
  }

  public int getEdad() {
    return edad;
  }

  public String getGenero() {
    return genero;
  }

  public String getDireccion() {
    return direccion;
  }

  public String getCiudad() {
    return ciudad;
  }

  public String getTelefono() {
    return telefono;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public void setCorreo(String correo) {
    this.correo = correo;
  }

  public void setEdad(int edad) {
    this.edad = edad;
  }

  public void setGenero(String genero) {
    this.genero = genero;
  }

  public void setDireccion(String direccion) {
    this.direccion = direccion;
  }

  public void setCiudad(String ciudad) {
    this.ciudad = ciudad;
  }

  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  @Override
  public String toString() {
    return "Usuario{" + "id=" + id + ", nombre='" + nombre + '\'' + ", correo='" + correo + '\''
        + ", edad=" + edad + ", genero='" + genero + '\'' + ", direccion='" + direccion + '\''
        + ", ciudad='" + ciudad + '\'' + ", telefono='" + telefono + '\'' + '}';
  }
}
