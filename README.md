# Proyecto CRUD de Usuarios

#### Este proyecto Java muestra un ejemplo de operaciones CRUD (Crear, Leer, Actualizar, Eliminar) en una base de datos MySQL. Utiliza JDBC para conectarse a la base de datos y realiza las operaciones en una tabla de usuarios.
#### Este proyecto fue creado con IntelliJ IDEA y utiliza Maven para administrar las dependencias(mysql).

## Estructura del Proyecto
### El proyecto se divide en tres clases principales:

1. DatabaseConnection
   Descripción: Esta clase se encarga de establecer la conexión con la base de datos MySQL.
   Métodos:
   getConnection(): Método estático que devuelve una instancia de conexión a la base de datos.
2. Usuario
   Descripción: Esta clase representa un usuario y se utiliza para crear objetos Usuario con
   información de usuario.
   Constructores:
   Usuario(String nombre, String correo, int edad, String genero, String direccion, String ciudad,
   String telefono): Constructor para crear un nuevo usuario.
   Usuario(int id, String nombre, String correo, int edad, String genero, String direccion, String
   ciudad, String telefono): Constructor para cargar un usuario existente desde la base de datos.
3. UsuarioDAO
   Descripción: Esta clase se encarga de realizar operaciones CRUD en la tabla de usuarios de la
   base de datos.
   Métodos:
   insertarUsuario(Usuario usuario): Inserta un nuevo usuario en la base de datos.
   actualizarUsuario(Usuario usuario): Actualiza la información de un usuario existente en la base
   de datos.
   eliminarUsuario(int usuarioId): Elimina un usuario de la base de datos por su ID.
   obtenerUsuarioPorId(int id): Obtiene un usuario de la base de datos por su ID.
   getUsuarios(): Obtiene todos los usuarios de la base de datos y muestra su información en la
   consola.

##   Uso del Proyecto
   El archivo Main demuestra el uso del proyecto con ejemplos de inserción, actualización,
   eliminación y selección de usuarios.
   
